/*
    This file is part of MolarCalc.

    MolarCalc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MolarCalc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MolarCalc.  If not, see <https://www.gnu.org/licenses/>.
    */

//Decimal comes from https://github.com/MikeMcl/decimal.js



//SERVICE WORKER
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register("sw.js")
        .then((reg) => console.log("ServiceWorker has been registered", reg))
        .catch((err) => console.log("ServiceWorker has NOT been registered", err))
} else {
    console.log("ServiceWorker is not supported by your browser");
}



currentMolarMass = new Decimal(0);
var textMolarMass = new Decimal(0);
var elementHistory = [];
var elementString;
var elementNumbers;
var periodicData;
window.onload = function () {
    const elementsDisplay = document.getElementById("elementsDisplay");
    const elementsDisplayTitle = document.getElementById("elementsDisplayTitle");
}



//Prevents SPACE from scrolling down and being inputed in fields, so that it can be used to clear
//Prevents BACKSPACE default behavior except for in input
window.onkeydown = function(e) {
    //32 is SPACE; 8 is BACKSPACE; can only compare one element
    if (e.keyCode == 32 || e.keyCode == 8 && event.target != document.getElementById("textInput") && event.target != document.getElementById("precisionInput")){
        e.preventDefault();
    }
};



//Begins by storing the json content for future use
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        periodicData = JSON.parse(this.responseText); //Turns the JSON file into an array
    }
}

xhttp.open("GET", "./data/elements.json", true); //Sets method to GET, path to elements.json & makes it asynchronous
xhttp.send(); //Sends the request





function submitTextInput(event) {
    event.preventDefault();
    if (event.detail == 0) {    //event.detail returns 0 if activated by keypress (ENTER) and 1 if by mouse click
        return;     //Prevent ENTER (and SPACE) from activating element buttons, to that they can be used for other functions
    }
    var inputString = document.getElementById("textInput").value;
    currentMolarMass = Decimal(0);
    textMolarMass = Decimal(0);
    elementHistory = [];

    //Look for parenthesis followed by numbers
    //The 'g' at the end is necessary to make it only return one array item for each, 
    //otherwise it will return what I believe is called a capturing group.
    parenthesisInput = inputString.match(/\(([^\)]+)\)[0-9]+/g);

    if (/\(([^\)]+)\)[0-9]+/.test(inputString)) {
        if (inputString.match(/\(/g).length == inputString.match(/\)/g).length) {
            try {
                //Send the string to the corresponding function
                currentMolarMass = advancedCalc(inputString, 1);   //1 as there is no coefficient (number following parenthesis)
                printMolarMass(currentMolarMass);
                //Make inputString numbers into subscript
                inputStringSubscript = inputString.replace(/[0-9]/g, "$&".sub()); //$& means the string that was matched (the number)
                //Display the string of elements
                elementsDisplay.innerHTML = inputStringSubscript;
                //Change the title to reflect that there are now selected elements
                elementsDisplayTitle.innerHTML = "Selected elements:";
            } catch (error) {
                console.log("Invalid input: " + error);
                clearElements();
                elementsDisplay.innerHTML = "<span style='color: red;'>Invalid input</span>";
            }
        } else {
            //Resets selection
            clearElements();
            //Display error message
            elementsDisplay.innerHTML = "<span style='color: red;'>Invalid parenthesis</span>";
            throw ("Invalid parenthesis");
        }  
    } else if (inputString == "") {
        //Clear display
        clearElements();
        //Display error message
        elementsDisplay.innerHTML = "<span style='color: red;'>No input</span>";
        throw ("No input");
    } else {
        //Push the string through the appropriate function
        textMolarMass = textInputHandling(inputString);
        //Print molar mass
        printMolarMass(textMolarMass);
        //Print the inputed string
        elementsDisplay.innerHTML = inputString.replace(/[0-9]/g, "$&".sub());
    }
}



//Detects key presses
function keyPressFunction(event) {
    //event.preventDefault();
    var key = event.which || event.keyCode;
    if (key == 46 || key == 32) {  //DEL or SPACE
        clearElements();
    } else if ((key == 81 || key == 8) && !currentMolarMass.equals(0) && event.target != document.getElementById("textInput") && event.target != document.getElementById("precisionInput")) {  //Q
        removeLastElement();
    }
}



//Called when the cursor moves onto the legend, to make all other element kinds gray
function legendColorChange(elementType, callingElement) {
    if (callingElement.checked == true){
        //Select all elements
        let list = document.querySelectorAll(".AM,.AEM,.L,.A,.TM,.PTM,.M,.RN,.NG,.U");
        //Select all elements of the chosen type (based on class)
        let selectedElements = document.querySelectorAll("." + elementType);

        //Go through and make all gray
        for (i = 0; i < list.length; i++) {
            list[i].classList.add("grayMode");
        }
        //Remove grayMode from the selected elements
        for (i = 0; i < selectedElements.length; i++) {
            selectedElements[i].classList.remove("grayMode");
        }
        //Uncheck all other legends
        for (let i = 0; i < 10; i++) {
            document.querySelectorAll(".legendCheckbox")[i].checked = false;
            callingElement.checked = true;
        }
    } else {
        //Remove grayMode from all elements
        let list = document.querySelectorAll(".AM,.AEM,.L,.A,.TM,.PTM,.M,.RN,.NG,.U");
        for (i = 0; i < list.length; i++) {
            list[i].classList.remove("grayMode");
        }
    }
}



//Called when the amount of significant figures is changed
function changePrecision() {
    //Calls the print function with the correct value
    if (!currentMolarMass.equals(0)) {
        printMolarMass(currentMolarMass);
    } else if (!textMolarMass.equals(0)) {
        printMolarMass(textMolarMass);
    }

}



//Called when an element is clicked to add its molar mass to the display
function addMolarMass(element) {
    if (event.detail == 0) {    //event.detail returns 0 if activated by keypress (ENTER) and 1 if by mouse click, works though deprecated
        return;     //Prevent ENTER (and SPACE) from activating element buttons, to that they can be used for other functions
    }
    //Resets the text molar mass variable as it is no longer in use
    textMolarMass = Decimal(0);
    //Add element to history array
    elementHistory.push(element);
    //Prints the selected elements
    printElementString();
    //Adds the new molar mass to the variable
    currentMolarMass = currentMolarMass.plus(getMolarMass(element));
    //Displays the new total molar mass
    printMolarMass(currentMolarMass);
}
















//F U N C T I O N S   O N L Y   C A L L E D   B Y   O T H E R   F U N C T I O N S



function textInputHandling(receivedString) {
    //Separate elements at capital letters
    receivedArray = receivedString.split(/(?=[A-Z])/);
    var internalMolarMass = new Decimal(0);

    for (let i = 0; i < receivedArray.length; i++) {   //Go through the whole array
        arrayContent = receivedArray[i];
        //If contains numbers
        if (/[0-9]/.test(arrayContent)) {
            //Separate numbers
            let quantity = arrayContent.split(/([0-9]+)/)[1];
            //Separate element
            let textInputElement = arrayContent.split(/(?=[0-9])/)[0];
            //Get and store molar mass and multiply
            let elementMolarMass = getMolarMass(textInputElement);
            internalMolarMass = internalMolarMass.plus(quantity * elementMolarMass); //Is elementMolarMass a Decimal (from getMolarMass) and will I need to use the .multiply function
        } else {
            //Get and store molar mass
            let elementMolarMass = getMolarMass(arrayContent);
            internalMolarMass = internalMolarMass.plus(elementMolarMass); //Check same as above
        }
    }
    return(internalMolarMass);
}



function printMolarMass(inputMolarMass) {
    precisionSetting = document.getElementById("precisionInput").value;
    if (1 > precisionSetting || precisionSetting > 10) {
        precisionSetting = 5; //If precisionSetting is not between 1 and 10, set it to the defult (5)
    }
    let x = new Decimal(inputMolarMass);
    //Rounds the answer (toSD) according to a setting (precisionSetting) and adds the unit (g/mol)
    document.getElementById("massDisplay").innerHTML = x.toSD(parseInt(precisionSetting)).toString() + " g/mol";
}



function getMolarMass(providedElement) {
    try {
        let elementData = periodicData.elements.find(elementData => elementData.symbol == providedElement); //Finds the object where the object 'symbol' matches 'element'
        retrievedMolarMass = Decimal(elementData.molarMass); //Adds the molar mass from the object previously found    
    } catch (error) {
        if (error == "TypeError: elementData is undefined") {
            incorrectElement = providedElement;
            //Clear the display
            clearElements();
            //Display the error message
            elementsDisplay.innerHTML = "<span style='color: red;'>" + incorrectElement + " is not an element." + "</span>";
            throw (incorrectElement + " is not an element.");
        }
    }
    return (retrievedMolarMass);
}



//Filters out all occurences of a certain element
function returnSameElements(elementHistory) {
    return elementHistory == element;
}



//Function that prints the currently selected elements in a string
function printElementString() {
    elementString = checkAmountFunction();

    //Prints out the selected elements and changes the title
    elementsDisplayTitle.innerHTML = "Selected elements:";
    elementsDisplay.innerHTML = elementString;
}



//Goes through the array elementHistory and creates a string where the amount of an element is
//summed up in notation like [element][amount of it in subscript] like H<sub>4</sub>
//The same element is separated into multiple "units" if there is another element in between
function checkAmountFunction() {
    let builtString = "";
    let i = 0;
    while (i < elementHistory.length) {
        let count = 1;
        for (k = i + 1; k < elementHistory.length; k++) {
            if (elementHistory[i] == elementHistory[k]) {
                count++;
            } else {
                break;
            }
        }
        if (count != 0) {
            builtString += elementHistory[i];
            if (count != 1) {
                builtString += "<sub>" + count + "</sub>";
            }
        }
        i = k;
    }
    return (builtString);
}



//Clears/purges all selected elements
function clearElements() {
    currentMolarMass = Decimal(0);
    elementHistory = [];
    printMolarMass(0);
    elementsDisplayTitle.innerHTML = "No elements have been selected.";
    elementsDisplay.innerHTML = "";
}



//Removes the last entered element.
function removeLastElement() {
    let element = elementHistory.pop();
    printElementString();
    currentMolarMass = currentMolarMass.minus(getMolarMass(element));
    printMolarMass(currentMolarMass);
    if (currentMolarMass.equals(0)) {
        elementsDisplayTitle.innerHTML = "No elements have been selected.";
    }
}



//Reworked version (v2) of text and parenthesis input handling
function advancedCalc (stringInput, coefficient) {
    if (/\(/.test(stringInput)) {
        firstParenthesis = stringInput.indexOf("(");
        lastParenthesis = stringInput.lastIndexOf(")");
        var insideParMolarMass;

        console.log("");    //Empty row
        console.log("Input: " + stringInput);    //Log input

        console.log("First parenthesis at: " + firstParenthesis);
        console.log("Last parenthesis at: "+ lastParenthesis);

        var insideParenthesis = stringInput.slice(firstParenthesis + 1, lastParenthesis)    //Stores everything inside, but excluding, the parenthesis
        console.log("Inside the parenthesis: " + insideParenthesis);

        var afterParenthesis = stringInput.slice(lastParenthesis);    //Only keeps everything from and including the last parenthesis
        var parenthesisCoefficient = afterParenthesis.match(/\)([0-9]+)/)[1]; //Only keeps the numbers after the parenthesis, is case of elements after it
        console.log("Coefficient is " + parenthesisCoefficient);

        var excludingParenthesis = stringInput.slice(0, firstParenthesis);  //Adds what in front of the parenthesis
        //Adds what's after the parenthesis
        excludingParenthesis += afterParenthesis.slice(afterParenthesis.match(/[A-Z]/).index);   //Find what's after the coefficient by slicing the string at the index where there is a capital letter
        console.log("Outside parenthesis: " + excludingParenthesis);

        var excParMolarMass = textInputHandling(excludingParenthesis);  //Get molar mass of elements outside parenthesis
        console.log("M outside parenthesis: " + excParMolarMass);
        var totalMolarMass = excParMolarMass;

        if (/\(/.test(insideParenthesis)) {
            insideParMolarMass = advancedCalc(insideParenthesis, parenthesisCoefficient).times(coefficient);
            console.log("M inside parenthesis * coefficient: " + insideParMolarMass);
            var totalMolarMass = totalMolarMass.plus(insideParMolarMass);
        } else {
            insideParMolarMass = textInputHandling(insideParenthesis).times(parenthesisCoefficient);  //Get molar mass and multiply with coefficient
            console.log("M (w/o inner p.) in p. * coefficient: " + insideParMolarMass);
            var totalMolarMass = totalMolarMass.plus(insideParMolarMass).times(coefficient);
        }
        console.log("M iteration: " + totalMolarMass);

        return totalMolarMass;
    }
}
            